# FUSS - Free Upgrade for a digitally Sustainable School

[FUSS](https://www.fuss.bz.it) è una soluzione GNU/Linux completa (server, client e desktop/standalone) basata su Debian per la gestione di una rete didattica.

## Repository

Il repository principale della distribuzione è attualmente su https://work.fuss.bz.it/projects

## Licenza 
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Installazione e uso
Per l'installazione e l'utilizzo di FUSS fare riferimento alla [documentazione](https://www.fuss.bz.it/page/doc/).
